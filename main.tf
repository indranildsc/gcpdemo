provider "google" {
  project = "iot-pipleline"
  region  = "us-central1"
  zone    = "us-central1-c"
}

//Create IOT simulator instances
resource "google_compute_instance" "iotsimulator" {
  name = "iotsimulator"
  machine_type = "f1-micro"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  metadata_startup_script = <<SCRIPT
    mkdir /demo
    chmod -R 777 /demo
SCRIPT

provisioner "file" {
  connection {
     type = "ssh"
     user = "demo"
     private_key = "${file("demo")}"
     agent = false
   }

  source = "sensor-data"
  destination = "/home/demo/"
}

provisioner "file" {
  connection {
     type = "ssh"
     user = "demo"
     private_key = "${file("demo")}"
     agent = false
   }

  source = "mqtt-script"
  destination = "/home/demo/"
}

provisioner "remote-exec" {
  connection {
     type = "ssh"
     user = "demo"
     private_key = "${file("demo")}"
     agent = false
   }
 inline = [
   "sudo cp -r /home/demo/sensor-data /demo/sensor-data",
   "sudo chmod -R 777 /demo/sensor-data",
   "sudo cp -r /home/demo/mqtt-script /demo/mqtt-script",
   "sudo chmod -R 777 /demo/mqtt-script",
   "chmod +x /demo/mqtt-script/initialsoftware.sh",
   "/demo/mqtt-script/initialsoftware.sh"
 ]
}

  metadata {
   sshKeys = "demo:${file("demo.pub")}"
 }


  network_interface {
    network       = "default"
    access_config = {
    }
  }
}

output "iotsimulatorip" {
 value = "${google_compute_instance.iotsimulator.network_interface.0.access_config.0.nat_ip}"
}



//Create table to store sensor data

resource "google_bigquery_dataset" "sensordataDS" {
  dataset_id                  = "weatherDS"
  friendly_name               = "weather-dataset"
  description                 = "Dataset to hold weathor sensor data"

  labels = {
    env = "iotdemo"
  }
}

resource "google_bigquery_table" "sensordata" {
  dataset_id = "${google_bigquery_dataset.sensordataDS.dataset_id}"
  table_id   = "sensordata"

  labels = {
    env = "iotdemo"
  }

  schema = "${file("weather.json")}"
}

resource "google_bigquery_table" "productQuality" {
  dataset_id = "${google_bigquery_dataset.sensordataDS.dataset_id}"
  table_id   = "productQuality"

  labels = {
    env = "iotdemo"
  }

  schema = "${file("productQuality.json")}"
}

//Create storage bucket

resource "google_storage_bucket" "weatherstorage" {
  name     = "weatherstorage"
}

resource "google_storage_bucket" "productqualitystorage" {
  name     = "productqualitystorage"
}

resource "google_storage_bucket_object" "loadQualityDataFunctionDef" {
  name   = "loadQualityDataFunctionDef"
  source = "./LoadQualityDataFunction.zip"
  bucket = "${google_storage_bucket.weatherstorage.name}"
}

resource "google_cloudfunctions_function" "loadQualityDataFunction" {
    name                      = "loadQualityDataFunction"
    entry_point               = "loadFile"
    event_trigger             = {
        event_type = "google.storage.object.finalize"
        resource   = "${google_storage_bucket.productqualitystorage.name}"
      }
    source_archive_bucket     = "${google_storage_bucket.weatherstorage.name}"
    source_archive_object     = "${google_storage_bucket_object.loadQualityDataFunctionDef.name}"
}

//Create pub sub topic

resource "google_pubsub_topic" "weatherStream" {
  name = "weatherStream"
}

resource "google_pubsub_subscription" "sensorDataForDB" {
  name  = "sensorDataForDB"
  topic = "${google_pubsub_topic.weatherStream.name}"
}

//Create cloud iot


resource "google_cloudiot_registry" "weather-registry" {
  name = "weather-registry"

  event_notification_config = {
    pubsub_topic_name = "${google_pubsub_topic.weatherStream.id}"
  }

  mqtt_config = {
    mqtt_enabled_state = "MQTT_ENABLED"
    http_enabled_state = "HTTP_DISABLED"
  }
}

//Dataflow: From Pub-sub to BigQuery
# resource "google_dataflow_job" "sensorDataToDBDataflow" {
#     region  = "us-west1"
#     name = "sensorDataToDBDataflow"
#     template_gcs_path = "gs://dataflow-templates/latest/PubSub_to_BigQuery"
#     temp_gcs_location = "gs://${google_storage_bucket.weatherstorage.id}/tmpDB"
#     parameters = {
#         inputTopic = "projects/iot-pipleline/topics/weatherStream"
#         outputTableSpec = "iot-pipleline:weatherDS.sensordata"
#     }
# }

//Dataflow: From Cloud Storage to PubSub - Do it manually with numworker=1
# resource "google_dataflow_job" "sensorDataFromStorageToPubsub" {
#     region  = "us-east1"
#     name = "sensorDataFromStorageToPubsub"
#     template_gcs_path = "gs://dataflow-templates/latest/Stream_GCS_Text_to_Cloud_PubSub"
#     temp_gcs_location = "gs://${google_storage_bucket.weatherstorage.id}/tmpPS"
#     parameters = {
#         inputFilePattern = "gs://codelab-iot-data-pipeline-sampleweatherdata/*.json"
#         outputTopic = "projects/iot-pipleline/topics/weatherStream"
#     }
# }
